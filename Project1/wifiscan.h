//Headers for WLAN SCAN
#pragma once
#include <winsock2.h>
#include <WS2tcpip.h>
#include <wlanapi.h>
#include <vector>
#include <string>
#include <windows.h>
#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <utility>
#include "dataanalysis.h"

#define FEATURE_SIZE 300		// input
//#define LABEL_COUNT 5			// output
#define TRAINNING_LIMIT 20
#define HIDDEN_LAYER 7

#define LOSS_THRESHOLD 5		// percentage of scan that sigStr go beyond boundary
#define SIGSTRG_LOW -100
#define SIGSTRG_HIGH 0
#define RELIABLE_COUNT_THRESHOLD 3
#define CONFIDENCE_THRESHOLD 0.7

#define DEFAULT_PORT "1310"
#define LOG_MODE_CVS 1
#define LOG_MODE_ONLINE 2
#define REGRESSION_MODE 1
#define CLASSIFICATION_MODE 2

using namespace alglib;
//#include <conio.h>
//#include <atlbase.h>
//#include <WinBase.h>
//#include <malloc.h>


// Lib paths
#pragma comment(lib, "wlanapi.lib")
#pragma comment(lib, "ole32.lib")

//variables used for WlanEnumInterfaces 
	PWLAN_INTERFACE_INFO_LIST	pIfList = NULL;
	PWLAN_INTERFACE_INFO		pIfInfo = NULL;
//variables used for WlanGetAvailableNetworkList
	PWLAN_AVAILABLE_NETWORK_LIST	pBssList = NULL;
    PWLAN_AVAILABLE_NETWORK			pBssEntry = NULL;
//VARIABLE USED FOR 
	 PWLAN_BSS_LIST					pWlanBssList=NULL;
	


// Variables for open handle
	DWORD pdwNegotiatedVersion	=0;
	HANDLE phClientHandle		=NULL;
	DWORD hResult				=ERROR_SUCCESS;
	DWORD pdwPrevNotifSource	= 0;

// GUID Variable
	GUID guidInterface			={0};

//Handles for events and threads
	HANDLE EventThread;
	HANDLE EventWait;
	DWORD tickWait = 0;
	DWORD wlanScanTick = 0;

// Mode
	int logMode; // 1 = XML, 2 = CVS
	int processMode; // 0 = Regression; 1 = Classification
	SOCKET s;
	int interfaceNumber = 0;
	int trainningLimit = TRAINNING_LIMIT;
	int normalizeTrainningLimit = 10;
	int bwait = 1;
	int scanIndex = 0;
	int reliableAPCount = 0;
	int oldScanIndex = -1;
	std::ofstream logfile, MACfile, testFile, resultFile, NN_File, DF_File;
	std::ifstream configFile, oldMACfile, trainningFile, inputNN, inputDF;
	bool MACListUpdated;
	bool isScanSuccess;
	double ConfidenceLvl;
	int featureSize;
	

//// Data logging
	std::vector<std::vector<double>> Scan_List;
	std::vector<const std::string> MAC_List;
	std::vector<const std::string> NR_RESULT;
	std::vector<const std::string> RF_RESULT;
	std::vector<int> guess_result;
	int *label;

	typedef struct{
		float x;
		float y;
	} POINT2D;
	std::vector<POINT2D>POINT_LIST;
	mlptrainer trn;
	multilayerperceptron network;
	mlpreport rep;

	decisionforest Df;
	dfreport dfReport;

	int LABEL_COUNT = 5;