#pragma once
#include <string>
#include <fstream>
#include <vector>
#include "dataanalysis.h"
using namespace alglib;

#define LOG_LIST_NAME "logList.txt"

class RawWiFiAnalyser
{
public:
	std::vector<std::vector<std::string>>listReliableMAC;
private:
	int numberOfLogFile, labelCount, trainingLimit;
	std::vector<real_2d_array> logDataList;
	std::vector<std::vector<std::string>> logMACList;
	std::vector<std::string>globalMACList;
public:
	RawWiFiAnalyser(int labelCount_, int trainingLimit_);
	~RawWiFiAnalyser();
	bool parseLogList();
	void analyzeData();
	int findMAC(std::string mac);
};

