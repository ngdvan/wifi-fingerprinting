#include "RawWiFiAnalyser.h"


RawWiFiAnalyser::RawWiFiAnalyser(int labelCount_, int trainingLimit_)
{
	this->labelCount = labelCount_;
	this->trainingLimit = trainingLimit_;
	this->listReliableMAC.resize(labelCount);
}


RawWiFiAnalyser::~RawWiFiAnalyser()
{
}


bool RawWiFiAnalyser::parseLogList(){
	std::ifstream logFile;
	logFile.open(LOG_LIST_NAME);
	if (logFile.fail()){
		return false;
	}

	std::string line;
	int indexLine = 0;
	/*structure file: 
	MAC
	Log*/
	while (logFile >> line){
		if (indexLine % 2 == 0){
			// MAC FILE
			std::ifstream MACFile;
			std::vector<std::string> MAC_List;
			MACFile.open(line.c_str());
			if (MACFile.fail()){
				return false;
			}
			int idx;
			std::string MACEntry;
			while (MACFile >> idx >> MACEntry){
				MAC_List.push_back(MACEntry);
			}
			MACFile.close();

			logMACList.push_back(MAC_List);
		}
		else{
			// LOG FILE
			std::ifstream logFile;
			logFile.open(line.c_str());
			if (logFile.fail()){
				return false;
			}
			std::string trainningData;
			logFile >> trainningData;
			real_2d_array xy(trainningData.c_str());
			logFile.close();
			logDataList.push_back(xy);
		}
		indexLine++;
	}
	numberOfLogFile = (int)logDataList.size();
}

void RawWiFiAnalyser::analyzeData(){
	
	for (int i = 0; i < numberOfLogFile; i++){
		int featureSize = logDataList[i].cols();
		for (int j = 0; j < featureSize; j++){
			
		}
	}
}


int RawWiFiAnalyser::findMAC(std::string mac){
	for (auto it = globalMACList.begin(); it != globalMACList.end(); it++){
		if ((*it) == mac){
			return it - globalMACList.begin();
		}
	}
	// othewise,push back new mac and return return end of vector idx
	globalMACList.push_back(mac);
	return globalMACList.end() - globalMACList.begin();
}