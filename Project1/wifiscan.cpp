#include "wifiscan.h"
#include <VersionHelpers.h>
#include <sstream>
#include <iomanip>


#pragma comment(lib,"ws2_32.lib") //Winsock Library


int findMAC(std::string mac){
	for (auto it = MAC_List.begin(); it != MAC_List.end(); it++){
		if ((*it) == mac){
			return it - MAC_List.begin();
		}
	}
	// othewise,push back new mac and return return end of vector idx
	if (logMode == LOG_MODE_ONLINE){
		return -1;
	}
	else{
		MAC_List.push_back(mac);
		return MAC_List.end() - MAC_List.begin();
	}
}

// function for bss list where we can read beacon with pointer to strcuture offset and length of response
void FuncWlanGetBSSList()
{
	hResult = WlanGetNetworkBssList(phClientHandle, 
									&guidInterface,
									NULL, 
								    dot11_BSS_type_any,
									NULL,
									NULL,
									&pWlanBssList);   
	if(hResult!= ERROR_SUCCESS)
	{
		printf("Failed WlanGetNetworkBssList  %d \n ",hResult);
		return;
	}
	//printf("no of memebers in bss list entry=%d  \n ",pWlanBssList->dwNumberOfItems);
		
	if(pWlanBssList->dwNumberOfItems==0)
	{
		printf("########FuncWlanGetBSSList is empty<---######## \n \n");	
		isScanSuccess = false;
		return;
	} 
	
	if (logMode == LOG_MODE_CVS || logMode == LOG_MODE_ONLINE){
		// create new scan entry
		std::vector<double> scanEntry;
		scanEntry.resize(featureSize);
		reliableAPCount = 0;
		fill(scanEntry.begin(), scanEntry.end(), -1);
		for (unsigned int i = 0; i < pWlanBssList->dwNumberOfItems; i++)
		{
			PWLAN_BSS_ENTRY pWlanBssEntry = &pWlanBssList->wlanBssEntries[i];
			if (pWlanBssEntry->dot11BssType == dot11_BSS_type_infrastructure){
				char tmp[20];
				sprintf(tmp, "%02x:%02x:%02x:%02x:%02x:%02x", pWlanBssEntry->dot11Bssid[0],
					pWlanBssEntry->dot11Bssid[1],
					pWlanBssEntry->dot11Bssid[2],
					pWlanBssEntry->dot11Bssid[3],
					pWlanBssEntry->dot11Bssid[4],
					pWlanBssEntry->dot11Bssid[5]);
				//std::cout << "Link Quality: " << pWlanBssEntry->uLinkQuality << std::endl;
				std::string macEntry(tmp);
				if (scanIndex == 0 && !MACListUpdated) {
					if (pWlanBssEntry->lRssi <= SIGSTRG_HIGH  && pWlanBssEntry->lRssi >= SIGSTRG_LOW){
						MAC_List.push_back(macEntry);
						scanEntry[i] = 1 - (pWlanBssEntry->lRssi * (-1)) / (double)100;
						reliableAPCount++;
						//scanEntry[i] = pWlanBssEntry->uLinkQuality;
					}
				}
				else{
					// put the sigstr to correct entry index
					if (pWlanBssEntry->lRssi <= SIGSTRG_HIGH && pWlanBssEntry->lRssi >= SIGSTRG_LOW){
						int idx = findMAC(macEntry);
						// in case found or add new in trainning phase.
						if (idx > -1 ){
							scanEntry[idx] = 1 - (pWlanBssEntry->lRssi * (-1)) / (double)100;;
							reliableAPCount++;
							//scanEntry[idx] = pWlanBssEntry->uLinkQuality;
						}
						else{
							// do nothing, discard.
						}
					}
				}
			}		
		}
		Scan_List.push_back(scanEntry);
	}
}

//thread created and running for event to be happen
DWORD WINAPI FuncWlanThread(LPVOID lpParameter)
{
	UNREFERENCED_PARAMETER(lpParameter);
	DWORD WaitResult;
	WaitResult = WaitForSingleObject(EventWait,INFINITE);   
	tickWait = GetTickCount() - tickWait;
	printf("wait time: %ld\t", (long)tickWait);
    switch (WaitResult) 
			{
				// Event object was signaled
				case WAIT_OBJECT_0: 
						//Un registering the wlan notification for scan as scan is completed
						hResult=WlanRegisterNotification(phClientHandle,
														WLAN_NOTIFICATION_SOURCE_NONE,
														TRUE,
														NULL,
														NULL,
														NULL,
														&pdwPrevNotifSource);
						
						if(hResult!=ERROR_SUCCESS)
							{
								printf("failed Wlan UnRegisterNotification=%d \n",hResult);
								return hResult;
							}
						else
						{
							FuncWlanGetBSSList();//working fine
						}
						
					break; 
				// An error occurred
				default: 
					printf("Wait error (%dn \n", GetLastError()); 
					return 0; 
			}
	bwait=0;
	return 1;
}

//function that creates thread and events and wait for unregistering the CB
int FuncWlanCreateThreadsAndEvents()
{
	tickWait = GetTickCount();
	EventWait=CreateEvent(NULL,
						  FALSE,
						  FALSE,
						  "WaitEvent");
	    if (EventWait == NULL) 
			{ 
				printf("CreateEvent failed (%d)\n", GetLastError());
				return -1;
			}
	EventThread=CreateThread(NULL,
							 0,
							 FuncWlanThread,
							 NULL,
							 0,
							 0);
		if (EventThread == NULL) 
		{ 
			printf("CreateThread failed (%d)\n", GetLastError());
			return -1;
		}
	return 1;
}

//wlan ACM notify function pointer for scan complete
void FuncWlanAcmNotify(PWLAN_NOTIFICATION_DATA data,PVOID context)
{
	if(data->NotificationCode==wlan_notification_acm_scan_complete)
	{
		wlanScanTick = GetTickCount() - wlanScanTick;
		printf("wlan scan tick: %ld \t", (long)wlanScanTick);
		if(!SetEvent(EventWait))
			printf("setting event failed \n");
	}

	if(data->NotificationCode==wlan_notification_acm_scan_fail)
	{
		if(!SetEvent(EventWait))
			printf(" setting event failed  \n");
	}	
}

//function to open and enumerate
int FuncWlanOpenAndEnum()
{
	DWORD dwClientVersion = (IsWindowsVistaOrGreater() ? 2 : 1);
	//creating session handle for the client to connect to server.
	hResult=WlanOpenHandle(dwClientVersion,NULL,&pdwNegotiatedVersion,&phClientHandle);
		if(hResult!=ERROR_SUCCESS)
			{
				printf("failed WlanOpenHandle=%d \n",hResult);
				return hResult;
			}
			else
				{
					//printf("WlanOpenHandle is success=%d \n",hResult);
				
				}

	//Enumerates all the wifi adapters currently enabled on PC.
	//Returns the list of interface list that are enabled on PC.
	hResult=WlanEnumInterfaces(phClientHandle,NULL,&pIfList);
			if(hResult!=ERROR_SUCCESS)
				{
					printf("failed WlanEnumInterfaces check adapter is on=%d \n",hResult);
					return hResult;
				}
			else
				{
					//printf("WlanEnumInterfaces is success=%d \n",hResult);
				
				}
	return hResult;
}

//planning to start scan operation
int FuncWlanScan()
{
		PWLAN_RAW_DATA	pIeData		=NULL;
		PBYTE			pData		=NULL;
	//printf("######## FuncWlanScan--->######## \n\n");
		
		wlanScanTick = GetTickCount();
	hResult=WlanRegisterNotification(phClientHandle,
									WLAN_NOTIFICATION_SOURCE_ALL,
									TRUE,
									(WLAN_NOTIFICATION_CALLBACK)FuncWlanAcmNotify,
									NULL,
									NULL,
									&pdwPrevNotifSource);
	if(hResult!=ERROR_SUCCESS)
		{
			printf("failed WlanRegisterNotification=%d \n",hResult);
			return hResult;
		}
	else
	{
		//printf("WlanRegisterNotification is success=%d \n",hResult);
	
	}
	//creating threads and events
	hResult=FuncWlanCreateThreadsAndEvents();
	if(hResult==-1)	
		{
			printf("failed FuncWlanCreateThreadsAndEvents=%d \n",hResult);
			return hResult;
		}

	//wlan scan without mentioning SSID. assuming PC has only one wifi card
	//PWLAN_RAW_DATA p = (PWLAN_RAW_DATA)malloc(4 + sizeof(DWORD));

	hResult=WlanScan(phClientHandle,&guidInterface,NULL,NULL,NULL);
	if(hResult!=ERROR_SUCCESS)
		{
			printf("failed WlanScan check adapter is enabled=%d \n ",hResult);
			if(!SetEvent(EventWait))
			{
				printf("setting event failed \n");
				return hResult;
			}
		}
	else
		{
		
			//printf("WlanScan with out IE is success=%d \n",hResult);
			
		}
	return hResult;
}

// Initialize socket for RTmaps communication
int socketInit(){
	WSADATA wsa;
	struct addrinfo *tcpResult = NULL,
		*ptr = NULL,
		hints;
	int iResult;

	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		return 0;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	iResult = getaddrinfo("127.0.0.1", DEFAULT_PORT, &hints, &tcpResult);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 0;
	}
	for (ptr = tcpResult; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		s = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (s == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}

		// Connect to server.
		iResult = connect(s, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(s);
			s = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(tcpResult);

	if (s == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 0;
	}
	return 1;
}

// parse configuration file
bool parseConfig(){
	configFile.open("config.txt");
	if (configFile.fail()){
		std::cout << "Cannot open config file";
		return false;
	}
	std::string argname, configData;

	//configFile.get
	while (configFile >> argname >> configData){
		if (argname == "MODE"){
			logMode = atoi(configData.c_str());
		}
		if (argname == "LABEL"){
			LABEL_COUNT = atoi(configData.c_str());
			label = new int[LABEL_COUNT];
			for (int i = 0; i < LABEL_COUNT; i++){
				label[i] = i;
			}
		}
		if (argname == "PROCESSMODE"){
			processMode = atoi(configData.c_str());
		}
		if (argname == "FEATURESIZE"){
			featureSize = atoi(configData.c_str());
		}
		if (argname == "MAC"){
			std::string oldMACfilename(configData);
			if (oldMACfilename == "n" || oldMACfilename == "N"){
				MACListUpdated = false;
			}
			else{
				MACListUpdated = true;
				MAC_List.clear();
				oldMACfile.open(oldMACfilename.c_str());
				if (oldMACfile.fail())
					return false;
				int idx;
				std::string MACEntry;
				while (oldMACfile >> idx >> MACEntry){
					MAC_List.push_back(MACEntry);
				}
				oldMACfile.close();
			}
		}
		if (argname == "INTERFACE"){
			interfaceNumber = atoi(configData.c_str());
		}
		if (argname == "TRAINNINGLIMIT"){
			trainningLimit = atoi(configData.c_str());
		}
		if (argname == "NORMALIZETRAINNINGLIMIT"){
			normalizeTrainningLimit = atoi(configData.c_str());
		}
		if (logMode == 2 && argname == "LOG"){
			std::string trainningFileName(configData);
			trainningFile.open(trainningFileName.c_str());
			if (trainningFile.fail()){
				return false;
			}				
		}
		if (logMode == 2 && argname == "CONFIDENCE"){
			ConfidenceLvl = atof(configData.c_str());
		}
	}
	// open socket
	if (!socketInit()){
		printf("socket failed!!\n");
		//return false;
	}
	std::cout << "PARSE CONFIG COMPLETE!" << std::endl;
	return true;
}

// normalize
real_2d_array processDataTrainning(real_2d_array xy_){
	int factor = (int)trainningLimit/normalizeTrainningLimit;
	real_2d_array nXY;
	nXY.setlength(normalizeTrainningLimit * LABEL_COUNT,featureSize +1);
	for (int i = 0; i < normalizeTrainningLimit * LABEL_COUNT; i++){
		for (int j = 0; j < featureSize+1; j++){
			// if not the label
			if (j != featureSize){
				for (int k = 0; k < factor; k++){
					nXY[i][j] += xy_[i + k*normalizeTrainningLimit][j];
				}
				nXY[i][j] = nXY[i][j] / (double)factor;
			}
			else{
				// keep the label
				int label = (int)i % normalizeTrainningLimit;
				int label_ = (int)xy_[i + (factor -1)*normalizeTrainningLimit][j];
				nXY[i][j] = label;
			}
		}
	}
	return nXY;
}

// Prepare input data + object depend on working mode
bool init(){
	
	//// ProcessMode
	//processMode = CLASSIFICATION_MODE;
	//// set logMode: 1 = XLM, 2 = CVS
	//std::cout << "Mode: 1 - OFFLINE LOG; 2 - ONLINE TEST: ";
	//std::cin >> logMode;

	//std::cout << "Label Count: ";
	//std::cin >> LABEL_COUNT;
	//label = new int[LABEL_COUNT];
	//for (int i = 0; i < LABEL_COUNT; i++){
	//	label[i] = i;
	//}

	//std::cout << "Process Mode: 1 - Regression Mode; 2 - Classification Mode ";
	//std::cin >> processMode;

	//// Update MAC_LIST
	//std::cout << "Old MAC File (n/N for no):";
	//std::string oldMACfilename;
	//std::cin >> oldMACfilename;
	//if (oldMACfilename == "n" || oldMACfilename == "N"){
	//	MACListUpdated = false;
	//}
	//else{
	//	MACListUpdated = true;
	//	MAC_List.clear();
	//	oldMACfile.open(oldMACfilename.c_str());
	//	if (oldMACfile.fail())
	//		return false;
	//	int idx;
	//	std::string MACEntry;
	//	while (oldMACfile >> idx >> MACEntry){
	//		MAC_List.push_back(MACEntry);
	//	}
	//	oldMACfile.close();
	//}
	//// open socket
	//if (!socketInit()){
	//	printf("socket failed!!\n");
	//	return false;
	//}
	featureSize = FEATURE_SIZE;

	if(!parseConfig())
		return false;
	// init and trainning of neural network
	if (logMode == LOG_MODE_ONLINE){
		inputNN.open("NNfile-CLASI.csv");
		inputDF.open("DFfile-CLASI.csv");
		if (!inputNN.fail() && !inputDF.fail()){
			std::string nnData((std::istreambuf_iterator<char>(inputNN)),
				std::istreambuf_iterator<char>());
			mlpunserialize(nnData, network);

			std::string dfData((std::istreambuf_iterator<char>(inputDF)),
				std::istreambuf_iterator<char>());
			dfunserialize(dfData, Df);
			printf("done loading network; start localization \n");
		}
		else if (!trainningFile.fail()){
			std::string trainningData;
			trainningFile >> trainningData;
			real_2d_array xy(trainningData.c_str());
			trainningFile.close();
			//real_2d_array nXY(processDataTrainning(xy));
			/*std::cout << "Confidence level:";
			std::cin >> ConfidenceLvl;*/

			std::cout << "Start Trainning Network" << std::endl;
			// fancy function to determine number of nodes in hiddle layer.
			int hidden_layer_nodes = (LABEL_COUNT + featureSize) / 3.0;
			// Neural Network
			if (processMode == REGRESSION_MODE){
				mlpcreatetrainer(featureSize, LABEL_COUNT, trn);
				mlpcreate1(featureSize, hidden_layer_nodes, LABEL_COUNT, network);
				mlpsetdataset(trn, xy, trainningLimit*LABEL_COUNT);
				mlptrainnetwork(trn, network, 10, rep);
			}
			else {
				mlpcreatetrainercls(featureSize, LABEL_COUNT, trn);
				mlpcreatec1(featureSize, hidden_layer_nodes, LABEL_COUNT, network);
				mlpsetdataset(trn, xy, trainningLimit*LABEL_COUNT);
				mlptrainnetwork(trn, network, 10, rep);
			}

			// Decision Forest
			int errCode = 0;
			dfbuildrandomdecisionforest(xy, trainningLimit*LABEL_COUNT, featureSize, LABEL_COUNT, 90, 0.3, errCode, Df, dfReport);

			// writing out network files
			char filename[50];
			if (processMode == REGRESSION_MODE){
				sprintf(filename, "NNfile-REGES.csv");
			}
			else {
				sprintf(filename, "NNfile-CLASI.csv");
			}
			NN_File.open(filename);
			std::string nn_structure;
			mlpserialize(network, nn_structure);
			NN_File << nn_structure;
			NN_File.close();

			if (processMode == REGRESSION_MODE){
				sprintf(filename, "DFfile-REGES.csv");
			}
			else {
				sprintf(filename, "DFfile-CLASI.csv");
			}
			DF_File.open(filename);
			std::string df_structure;
			dfserialize(Df, df_structure);
			DF_File << df_structure;
			DF_File.close();
			printf("done trainning/ writing network; start localization \n");
		}
		inputNN.close();
		inputDF.close();
		
	}
	if (logMode == LOG_MODE_CVS){
		SYSTEMTIME st;
		GetSystemTime(&st);
		char filename[50];
		if (processMode == REGRESSION_MODE){
			sprintf(filename, "logfile-REGES-%d-%02d%02d%02d.%03d.csv", logMode, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		}
		else {
			sprintf(filename, "logfile-CLASI-%d-%02d%02d%02d.%03d.csv", logMode, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		}
		logfile.open(filename);
		if (logfile.fail())
			return false;
		sprintf(filename, "MACfile-%d-%02d%02d%02d.%03d.csv", logMode, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		MACfile.open(filename);
		if (MACfile.fail())
			return false;
	}
	return true;
}

// Log file depends on working mode
void writingLog(){
	int refinedFeatureSize = MAC_List.size();

	// Post Processing for offline log CVS
	if (logMode == LOG_MODE_CVS){
		

		// start writing MAC file
		int idxMAC = 0;
		for (auto it = MAC_List.begin(); it != MAC_List.end(); it++){
			// eval sigstr of that MAC
			int packageLostCounter = 0;
			for (int i = 0; i < (int)Scan_List.size(); i++){
				if (Scan_List[i][idxMAC] == -100){
					packageLostCounter++;
				}
			}
			float percentageLost = ((float)packageLostCounter / Scan_List.size()) * 100;
			MACfile << idxMAC << " " << (*it) << "\n";
			idxMAC++;
		}
		for (auto it = Scan_List.begin(); it != Scan_List.end(); it++){
			(*it).resize(refinedFeatureSize);
		}

		// start writing log file
		logfile << "[";
		for (auto it = Scan_List.begin(); it != Scan_List.end(); it++){
			if (it == Scan_List.begin())
				logfile << "[";
			else
				logfile << ",[";
			for (int i = 0; i < refinedFeatureSize; i++){
				logfile << (*it)[i] << ",";
			}
			for (int i = 0; i < LABEL_COUNT; i++){
				if (it - Scan_List.begin() < trainningLimit * (i + 1) && it - Scan_List.begin() >= trainningLimit*i){
					if (processMode == CLASSIFICATION_MODE){
						logfile << label[i] << "]";
						break;
					}
					else{
						logfile << std::setfill('0') << std::setprecision(3) << POINT_LIST[i].x << "," << POINT_LIST[i].y << "]";
						break;
					}
				}
			}
		}
		logfile << "]";
		// end of log file		
		logfile.close();
		MACfile.close();
	}
	else if (logMode == LOG_MODE_ONLINE){
		SYSTEMTIME st;
		GetSystemTime(&st);
		char filename[50];
		
		if (processMode == REGRESSION_MODE){
			sprintf(filename, "resultfile-REGES-%d-%02d%02d%02d.%03d.csv", logMode, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		}
		else {
			sprintf(filename, "resultfile-CLASI-%d-%02d%02d%02d.%03d.csv", logMode, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		}
		resultFile.open(filename);
		if (resultFile.fail())
			return;

		if (processMode == REGRESSION_MODE){
			sprintf(filename, "testfile-REGES-%d-%02d%02d%02d.%03d.csv", logMode, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		}
		else {
			sprintf(filename, "testfile-CLASI-%d-%02d%02d%02d.%03d.csv", logMode, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		}
		testFile.open(filename);
		if (testFile.fail())
			return;
		testFile << "[\n";
		for (auto it = Scan_List.begin(); it != Scan_List.end(); it++){
			if (it == Scan_List.begin())
				testFile << "[";
			else
				testFile << ",[";
			for (int i = 0; i < refinedFeatureSize; i++){
				if (i == refinedFeatureSize - 1)
					testFile << (*it)[i] << "]\n";
				else
					testFile << (*it)[i] << ",";
			}
		}
		testFile << "]\n";

		for (int i = 0; i < scanIndex; i++){
			resultFile << i << ", " << guess_result[i] << std::endl;
			/*resultFile << NR_RESULT[i] << std::endl;
			resultFile << RF_RESULT[i] << std::endl;*/
		}
		testFile.close();
		resultFile.close();
	}
}

// main function
int main(int argc, char *argv[])
{
	if (!init()){
		//return -1;
	}

	//openin handle for client to communicate with adapter and enumerating the wifi interfaces
	hResult = FuncWlanOpenAndEnum();
	if (hResult != ERROR_SUCCESS)
	{
		printf("######## failed FuncWlanOpenAndEnum=%d  \n", hResult);
		WlanCloseHandle(phClientHandle, NULL);
		return hResult;
	}
	
	//Choose Wifi Card
	guidInterface = pIfList->InterfaceInfo[interfaceNumber].InterfaceGuid;
	
	isScanSuccess = true;
	DWORD timeLog = 0;
	bool stopCondition = false;
	while (!stopCondition){
		timeLog = GetTickCount();
		//guidInterface = pIfList->InterfaceInfo[0].InterfaceGuid;
		hResult = FuncWlanScan();
		/*
		guidInterface = pIfList->InterfaceInfo[1].InterfaceGuid;
		hResult = FuncWlanScan();*/

		if (hResult != ERROR_SUCCESS)
		{
			printf("failed FuncWlanScan\n");
			isScanSuccess = false;
			WlanCloseHandle(phClientHandle, NULL);
			WlanFreeMemory(pIfList);
			CloseHandle(EventWait);
			logfile.close();
			MACfile.close();
			return hResult;
		}
		while (bwait)
		{
			Sleep(50);
		}
		if (isScanSuccess){
			// for sending info over TCP to RTmaps
			timeLog = GetTickCount() - timeLog;
			printf("INDEX SCAN --------------- %d in: %d \n", scanIndex, timeLog);

			// Always send ScanIndex over TCP for RTMaps
			if (oldScanIndex != scanIndex){
				/*char numb[10];
				_itoa(scanIndex, numb, 10);
				if (send(s, numb, sizeof(numb), 0) < 0){
					printf("send error");
					break;
				}*/
				oldScanIndex = scanIndex;
			}
			
			// MODE CSV LABELING, PREPARE TRAINNING DATA
			if (scanIndex != 0 && (scanIndex + 1) % trainningLimit == 0 && logMode == LOG_MODE_CVS){
				std::cout << '\a';
				if (processMode == REGRESSION_MODE){
					POINT2D p;
					std::cout << "enter this location x,y: ";
					std::cin >> p.x >> p.y;
					POINT_LIST.push_back(p);
				}
				else{
					std::cout<<"Done with Pos: "<< scanIndex << " Move to the next Position";
					std::string stupidPause;
					std::cin >> stupidPause;
				}
			}

			// MODE ONLINE PROCESS DATA
			if (logMode == LOG_MODE_ONLINE){
				// forming feature vector from scanning result
				if (reliableAPCount > RELIABLE_COUNT_THRESHOLD){
					std::ostringstream  vectorData;
					vectorData.clear();
					vectorData << "[";
					for (int i = 0; i < featureSize; i++){
						if (i == featureSize - 1)
							vectorData << Scan_List[scanIndex][i];
						else
							vectorData << Scan_List[scanIndex][i] << ",";
					}
					vectorData << "]";
					real_1d_array x(vectorData.str().c_str());
					real_1d_array y_nr;
					DWORD processTime = GetTickCount();
					mlpprocess(network, x, y_nr);
					//printf("NEURAL NETWORK: ScanIndex: %d	Result: %s\n", scanIndex, y_nr.tostring(1).c_str());
					NR_RESULT.push_back(y_nr.tostring(1));
					//std::cout << "NEURAL TIME: " << GetTickCount() - processTime << std::endl;
					processTime = GetTickCount();
					real_1d_array y_rf;
					dfprocess(Df, x, y_rf);
					//printf("RANDOM FOREST : ScanIndex: %d	Result: %s\n", scanIndex, y_rf.tostring(1).c_str());
					RF_RESULT.push_back(y_rf.tostring(1));
					//std::cout << "FOREST TIME: " << GetTickCount() - processTime << std::endl;
					double maxProb = -1;
					int maxProbId = -1;

					for (int id = 0; id < y_rf.length(); id++){
						//std::cout << y_nr[id] << " and " << y_rf[id] << std::endl;
						double positionProb = (y_rf[id] + y_nr[id])/2;
						if (positionProb >= maxProb){
							maxProb = positionProb;
							maxProbId = id;
						}
					}
					if (maxProb >= ConfidenceLvl){
						guess_result.push_back(maxProbId);
						//printf("INDEX: %d -- BEST GUEST at Location: %d from %d second ago \n", scanIndex, maxProbId+1, timeLog);
						printf("-----------------BEST GUEST at Location : %d from %d second ago--------------\n", maxProbId + 1, timeLog);
					}
					else{
						guess_result.push_back(-1);
						printf("NOT RELIABLE at %0.2f\n",maxProb);
					}
				}
				else{
					std::cout << "UNRELIABLE SCAN \n";
				}
			}

			scanIndex++;
		}
		isScanSuccess = true;
		bwait = 1;
		if (scanIndex >= trainningLimit*LABEL_COUNT && logMode != LOG_MODE_ONLINE || GetAsyncKeyState(VK_ESCAPE) )
			stopCondition = true;

	}
	writingLog();
	
	WlanFreeMemory(pIfList);
	WlanCloseHandle(phClientHandle,NULL);
	shutdown(s, SD_SEND);
	closesocket(s);
	return 0;
}